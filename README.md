# Projetos da disciplina 


Projetos desenvolvidos durante 2020.2 na disciplina Tecnicas de Desenvolvimento de Algoritmos com professor Leandro:

  - lista 2
  - lista 3
  - lista 4
  - lista 5
  - jogo da forca

# Como usar

  - baixar uma IDE (devC++, codeblocks, vs code.. )
  - abrir os arquivos no compilador 
  - compilar e executar
  

Você também pode testar online:
  - copiar e colar o codigo em algum compilador online(Codechef, Repl.it,  OnlineCompiler...)



### Autor

    Iahgo Souza Barros
    contato: iahgo@icloud.com





    




